# nodeのltsを指定
FROM node:12.13.0-alpine
WORKDIR /var/www/app
COPY ./package*.json ./

RUN apk update && \
    npm install && \
    npm install -g npm

#COPY . .
#RUN npm run build
#CMD ["/bin/ash"]
CMD ["npm", "run", "serve"]

